package newpackage;

class LogicalOperators {

    public static void main(String[] args) {
        //Logical Operators Logical And&&   Logical OR|| Logical NOt !
        boolean b = 10 > 0 && 3 < 20;
        boolean c = 0 > 20 && 3 < 20;
        boolean d = 3 < 20 && 0 > 20;
        boolean n = 21 < 20 && 0 > 20;
        boolean e = 10 > 0 || 3 < 20;
        boolean f = 0 > 20 || 3 < 20;
        boolean g = 3 < 20 || 0 > 20;
        boolean h = 10 < 10 || 10 > 20;
        System.out.println(b);
        System.out.println(c);
        System.out.println(d);
        System.out.println(n);
        System.out.println(e);
        System.out.println(f);
        System.out.println(g);
        System.out.println(h);
        int y = 20, z;
        double p = (y / 3 * (56 + 78));  //first calculates 56+78=134 20/3=6 6*134=804
        System.out.println(p);
    }
}
