package newpackage1;

import java.util.Scanner;

public class SelectionSort {

    public static void ascendingOrder(int a[]) {
        int min;
        for (int i = 0; i < a.length - 1; i++) {
            min = i;
            for (int j = i + 1; j < a.length; j++) {
                if (a[j] < a[i]) {
                    min = j;
                }
            }
            int temp = a[i];
            a[i] = a[min];
            a[min] = temp;
        }
    }

    static void display(int a[]) {
        for (int i : a) {
            System.out.println(i);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter range");
        int n = sc.nextInt();
        int[] a = new int[n];
        System.out.println("Enter the elements");
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        ascendingOrder(a);
        System.out.println("After sortnig the array");
        display(a);
    }
}
