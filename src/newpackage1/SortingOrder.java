package newpackage1;

import java.util.*;

public class SortingOrder {

    static void ascendingOrder(int arr[]) {
        int n = arr.length;
        int count = 0;
        boolean swapped = false;
        //int[] arr={10,50,20,25,2};
        for (int i = 0; i < n - 1; i++) {
            for (int j = 0; j < n - i - 1; j++) {
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                    swapped = true;
                }
                count++;
            }
            if (!swapped) {
                break;
            }
            swapped = false;
        }
        System.out.println("count =" + count);
    }

    static void displayArray(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    static void descendingOrder(int arr[]) {
        int n = arr.length;
        for (int i = n - 1; i >= 0; i--) {
            System.out.println(arr[i]);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the range");
        int n = sc.nextInt();
        int[] a = new int[n];
        System.out.println("Enter the elements of the array");
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
//        System.out.println("before sorting the array");
        ascendingOrder(a);
        System.out.println("Ascending Oreder");
        for (int i = 0; i < n; i++) {
            System.out.println(a[i]);
        }
        System.out.println("Descending order");
        descendingOrder(a);
    }

}
