package newpackage1;

import java.util.*;

public class InsertionOrder {

    public static void ascendingOrder(int[] arr) {    //i=2//0  1  2  3  4
        for (int i = 1; i < arr.length; i++) {     //arr[5]={50,40,30,20,10};
            int j;                
            int temp = arr[i];     //temp=30
            for (j = i; (j > 0) && (temp < arr[j - 1]); j--) {  //j=2   j>0 &&  30<40      //arr[40,50,30,20,10]
                 arr[j] = arr[j - 1];      //arr[2]=arr[1]       arr[5]={40,50,50,20,10} 
            }
            arr[j] = temp; 
        }
    }

    static void display(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            System.out.println(arr[i]);
        }
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("enter range");
        int n = sc.nextInt();
        int[] a = new int[n];
        System.out.println("Enter the elements");
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        System.out.println("after sorting the elements");
        ascendingOrder(a);
        display(a);
    }
}
