package newpackage1;

public class MatrixAddition {

    static void matrixAddition(int[][] a, int[][] b, int[][] c) {

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a[i].length; j++) {
                c[i][j] = a[i][j] + b[i][j];
            }
        }
    }

    static void display(int[][] c) {
        for (int i[] : c) {
            for (int j : i) {
                System.out.print(j+" ");
            }
            System.out.println("");
        }
    }

    public static void main(String[] args) {
        int[][] a = {{8, 18, 8}, {9, 0, 0}, {9, 2, 4}, {4, 9, 0}, {2, 3, 4}};
        int[][] b = {{8, 18, 8}, {9, 0, 0}, {9, 2, 4}, {4, 9, 0}, {2, 3, 4}};
        int[][] c = new int[5][3];
        matrixAddition(a, b, c);
        System.out.println("After Adding Matrix");
        display(c);
    }
}
